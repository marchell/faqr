package faqr

import (
	"time"

	"gitlab.com/marchell/faqr/number"
	faqr_time "gitlab.com/marchell/faqr/time"
)

type Faqr interface {
	// set new seed for the random number generator
	Seed(seed int64)

	// set a new time reference to use when generating date-time related things
	// this is useful for testing with a fixed time
	ReferenceTime(now time.Time)

	// set a new time reference generator to use when generating date-time related things
	// this is useful for testing with continously changing time
	ReferenceTimeFn(fn func() time.Time)

	Time() faqr_time.Time

	Number() number.Number
}
