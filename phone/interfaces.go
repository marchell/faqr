package phone

import (
	"gitlab.com/marchell/faqr/opts"
)

type Phone interface {
	Imei() string

	// Returns a random phone number.
	// opts["format"] - string, '501-###-###' => '501-039-841', '+48 91 ### ## ##' => '+48 91 463 61 70'
	Number(options ...opts.Optionable) string
}
