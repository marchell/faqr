# faqr

Make fake data for golang.

Copying <https://fakerjs.dev/>, a subset of it.

## Contribution

Make sure test is passing, then make PR

## Todo

### Interfaces

#### TODO

- [Location](https://fakerjs.dev/api/location.html)
- [Finance](https://fakerjs.dev/api/finance.html)
- [Image](https://fakerjs.dev/api/image.html)
- [Internet](https://fakerjs.dev/api/internet.html)
- [Company](https://fakerjs.dev/api/company.html)
- [System](https://fakerjs.dev/api/system.html)
- [Database](https://fakerjs.dev/api/database.html)
- [Vehicle](https://fakerjs.dev/api/vehicle.html)
- [Commerce](https://fakerjs.dev/api/commerce.html)
- [Science](https://fakerjs.dev/api/science.html)
- [Animal](https://fakerjs.dev/api/animal.html)
- [Git](https://fakerjs.dev/api/git.html)
- [Hacker](https://fakerjs.dev/api/hacker.html)

### Implementations

- [String](https://fakerjs.dev/api/string.html)
- [Date](https://fakerjs.dev/api/date.html)
- [Datatype](https://fakerjs.dev/api/datatype.html)
- [Helpers](https://fakerjs.dev/api/helpers.html)
- [Word](https://fakerjs.dev/api/word.html)
- [Lorem](https://fakerjs.dev/api/lorem.html)
- [Color](https://fakerjs.dev/api/color.html)
- [Phone](https://fakerjs.dev/api/phone.html)
- [Person](https://fakerjs.dev/api/person.html)

[//]: # (MVP milestone)

- [Location](https://fakerjs.dev/api/location.html)
- [Finance](https://fakerjs.dev/api/finance.html)
- [Image](https://fakerjs.dev/api/image.html)
- [Internet](https://fakerjs.dev/api/internet.html)
- [Company](https://fakerjs.dev/api/company.html)
- [System](https://fakerjs.dev/api/system.html)
- [Database](https://fakerjs.dev/api/database.html)
- [Vehicle](https://fakerjs.dev/api/vehicle.html)
- [Commerce](https://fakerjs.dev/api/commerce.html)
- [Science](https://fakerjs.dev/api/science.html)
- [Animal](https://fakerjs.dev/api/animal.html)
- [Git](https://fakerjs.dev/api/git.html)
- [Hacker](https://fakerjs.dev/api/hacker.html)
- See something interesting in [another faker](https://github.com/dmgk/faker)
