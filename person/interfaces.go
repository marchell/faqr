package person

import (
	"gitlab.com/marchell/faqr/opts"
)

type Person interface {
	Bio() string

	FirstName(options ...opts.Optionable) string

	FullName(options ...opts.Optionable) string

	Gender() string

	JobArea() string

	JobDescriptor() string

	JobTitle() string

	JobType() string

	LastName(options ...opts.Optionable) string

	MiddleName(options ...opts.Optionable) string

	Prefix() string

	Sex() string

	Suffix() string

	ZodiacSign() string
}
