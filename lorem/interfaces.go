package lorem

import (
	"gitlab.com/marchell/faqr/opts"
)

type Lorem interface {
	// Generates the given number lines of lorem separated by '\n'.
	// opts["count"] - the number of lines to generate, default is 1
	// opts["min"] and opts["max"] - the number of lines range, min and max inclusive, only used when count is not set
	Lines(options ...opts.Optionable) string

	// Generates a paragraph with the given number of sentences.
	// opts["count"] - the number of sentences to generate, default is 3
	// opts["min"] and opts["max"] - the number of sentences range, min and max inclusive, only used when count is not set
	Paragraph(options ...opts.Optionable) string

	// Generates some paragraph.
	// opts["count"] - the number of paragraphs to generate, default is 3
	// opts["min"] and opts["max"] - the number of paragraphs range, min and max inclusive, only used when count is not set
	// separator - the separator between paragraphs, default is "\n"
	Paragraphs(options ...opts.Optionable) string

	// Generates a space separated list of words beginning with a capital letter and ending with a period.
	// opts["wordCount"] - the number of word in sentences to generate, default is 3
	// opts["wordCountMin"] and opts["wordCountMax"] - the number of word in sentences range, min and max inclusive, only used when wordCount is not set
	Sentence(options ...opts.Optionable) string

	// Generates sentences.
	// opts["sentenceCount"] - the number of sentences to generate, default is 3
	// opts["sentenceCountMin"] and opts["sentenceCountMax"] - the number of sentences range, min and max inclusive, only used when sentenceCount is not set
	// separator - the separator between sentences, default is " "
	Sentences(options ...opts.Optionable) string

	// Generates a slugified text consisting of the given number of hyphen separated words.
	// opts["wordCount"] - the number of words to generate, default is 3
	// opts["wordCountMin"] and opts["wordCountMax"] - the number of words range, min and max inclusive, only used when wordCount is not set
	Slug(options ...opts.Optionable) string

	// Generates a random text based on a random lorem method.
	Text(options ...opts.Optionable) string

	// Generates a word of a specified length.
	// opts["length"] - the length of the word to generate, default is 5
	// opts["lengthMin"] and opts["lengthMax"] - the length of the word range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Default is "random". Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Word(options ...opts.Optionable) string

	// Generates words.
	// opts["wordCount"] - the number of words to generate, default is 3
	// opts["wordCountMin"] and opts["wordCountMax"] - the number of words range, min and max inclusive, only used when wordCount is not set
	Words(options ...opts.Optionable) string
}
