package faqr

import "time"

// deafult instance so user can convenincely call functions
// without building a new instance
var defaultFaqr = NewFaqr()

// change the default instance's seed
func Seed(seed int64) {
	defaultFaqr.Seed(seed)
}

func ReferenceTime(now time.Time) {
	defaultFaqr.ReferenceTime(now)
}

func ReferenceTimeFn(fn func() time.Time) {
	defaultFaqr.ReferenceTimeFn(fn)
}
