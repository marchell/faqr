package helper

import (
	"gitlab.com/marchell/faqr/number"
	"gitlab.com/marchell/faqr/opts"
	"gitlab.com/marchell/faqr/utils"
)

type HelperFaker[V comparable] struct {
	Number number.Number
}

func NewHelper(num number.Number) Helper {
	return &HelperFaker{
		Number: num,
	}
}

func (f *HelperFaker[V]) ArrayElement(array []V) V {
	panic("not implemented") // TODO: Implement
}

func (f *HelperFaker[V]) ArrayElements(array []V) []V {
	panic("not implemented") // TODO: Implement
}

// do callback with probability
func (f *HelperFaker[V]) Maybe(fn func() V, probability float64) V {
	panic("not implemented") // TODO: Implement
}

func (f *HelperFaker[V]) Multiple(generator func() V, options ...opts.Optionable) []V {
	optionMap := utils.FilterMapByKey(utils.OptionsToMap(options), "count", "max", "min")
	count := uint(3)

	_, hasMax := optionMap["max"]
	_, hasMin := optionMap["min"]

	if value, found := optionMap["count"]; found {
		v, isInt := value.(uint)
		if isInt {
			count = v
		}
	} else if hasMax || hasMin {
		count = f.Number.Uint(options...)
	}

	result := make([]V, count)

	for i := uint(0); i < count; i += 1 {
		result[i] = generator()
	}

	return result
}

// # will be replaced with a digit (0 - 9).
// ? will be replaced with an upper letter ('A' - 'Z')
// and * will be replaced with either a digit or letter.
func (f *HelperFaker[V]) ReplaceSymbols(str string) string {
	panic("not implemented") // TODO: Implement
}

func (f *HelperFaker[V]) Shuffle(array []V) []V {
	panic("not implemented") // TODO: Implement
}

func (f *HelperFaker[V]) Slugify(str string) string {
	panic("not implemented") // TODO: Implement
}

func (f *HelperFaker[V]) RangeToNumber(options ...opts.Optionable) int64 {
	optionMap := utils.FilterMapByKey(
		utils.OptionsToMap(options),
		"length",
		"max",
		"min",
	)

	length, hasLength := optionMap["length"]

	if hasLength {
		return length.(int64)
	}

	return f.Number.Int64(options...)
}
