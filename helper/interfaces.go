package helper

import "gitlab.com/marchell/faqr/opts"

type Helper[V comparable] interface {
	ArrayElement(array []V) V

	ArrayElements(array []V) []V

	// do callback with probability
	Maybe(fn func() V, probability float64) V

	Multiple(generator func() V, options ...opts.Optionable) []V

	//# will be replaced with a digit (0 - 9).
	// ? will be replaced with an upper letter ('A' - 'Z')
	// and * will be replaced with either a digit or letter.
	ReplaceSymbols(str string) string

	Shuffle(array []V) []V

	Slugify(str string) string

	RangeToNumber(options ...opts.Optionable) int64
}
