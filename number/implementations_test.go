package number

import (
	"testing"

	"gitlab.com/marchell/faqr/opts"
)

func TestNumeric(t *testing.T) {
	type args struct {
		defaultMax int
		defaultMin int
		options    []opts.Optionable
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "no option passed",
			args: args{
				defaultMin: -1,
				defaultMax: 5,
				options: []opts.Optionable{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Numeric(tt.args.defaultMax, tt.args.defaultMin, tt.args.options...)

			if got > tt.args.defaultMax && got < tt.args.defaultMin {
				t.Errorf("Numeric() = %v, max = %v, min =%v", got, tt.args.defaultMax, tt.args.defaultMin)
			}
		})
	}
}
