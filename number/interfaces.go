package number

import "gitlab.com/marchell/faqr/opts"

type Number interface {
	// generate a random number
	Int(options ...opts.Optionable) int
	Int8(options ...opts.Optionable) int8
	Int16(options ...opts.Optionable) int16
	Int32(options ...opts.Optionable) int32
	Int64(options ...opts.Optionable) int64

	Uint(options ...opts.Optionable) uint
	Byte(options ...opts.Optionable) byte
	Uint8(options ...opts.Optionable) uint8
	Uint16(options ...opts.Optionable) uint16
	Uint32(options ...opts.Optionable) uint32
	Uint64(options ...opts.Optionable) uint64

	Float32(options ...opts.Optionable) float32
	Float64(options ...opts.Optionable) float64

	Binary(options ...opts.Optionable) string
	Octal(options ...opts.Optionable) string
}
