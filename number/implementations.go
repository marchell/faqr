package number

import (
	"math"
	"math/rand"
	"strconv"

	"gitlab.com/marchell/faqr/clock"
	"gitlab.com/marchell/faqr/opts"
	"gitlab.com/marchell/faqr/utils"
	"golang.org/x/exp/constraints"
)

type NumberFaker struct{}

func Numeric[T constraints.Integer | constraints.Float](defaultMax T, defaultMin T, options ...opts.Optionable) T {
	optionMap := utils.FilterMapByKey(utils.OptionsToMap(options), "max", "min")

	max := defaultMax
	min := defaultMin

	_, hasMax := optionMap["max"]
	_, hasMin := optionMap["min"]

	optionMax, maxTypeOk := optionMap["max"].(T)
	optionMin, minTypeOk := optionMap["min"].(T)

	if hasMax && maxTypeOk {
		max = optionMax
	}
	if hasMin && minTypeOk {
		min = optionMin
	}

	if max == min {
		return max
	}

	if max < min {
		panic("min greater than max")
	}

	r := rand.New(rand.NewSource(clock.Clock.Now().UnixNano()))

	return T(min + T(r.Float64()*float64(max-min)))
}

func (f *NumberFaker) Int(options ...opts.Optionable) int {
	return Numeric[int](math.MaxInt, math.MinInt, options...)
}

func (f *NumberFaker) Int8(options ...opts.Optionable) int8 {
	return Numeric[int8](math.MaxInt8, math.MinInt8, options...)
}

func (f *NumberFaker) Int16(options ...opts.Optionable) int16 {
	return Numeric[int16](math.MaxInt16, math.MinInt16, options...)
}

func (f *NumberFaker) Int32(options ...opts.Optionable) int32 {
	return Numeric[int32](math.MaxInt32, math.MinInt32, options...)
}

func (f *NumberFaker) Int64(options ...opts.Optionable) int64 {
	return Numeric[int64](math.MaxInt64, math.MinInt64, options...)
}

func (f *NumberFaker) Uint(options ...opts.Optionable) uint {
	return Numeric[uint](math.MaxUint, 0, options...)
}

func (f *NumberFaker) Byte(options ...opts.Optionable) byte {
	return Numeric[byte](math.MaxUint8, 0, options...)
}

func (f *NumberFaker) Uint8(options ...opts.Optionable) uint8 {
	return Numeric[uint8](math.MaxUint8, 0, options...)
}

func (f *NumberFaker) Uint16(options ...opts.Optionable) uint16 {
	return Numeric[uint16](math.MaxUint16, 0, options...)
}

func (f *NumberFaker) Uint32(options ...opts.Optionable) uint32 {
	return Numeric[uint32](math.MaxUint32, 0, options...)
}

func (f *NumberFaker) Uint64(options ...opts.Optionable) uint64 {
	return Numeric[uint64](math.MaxUint64, 0, options...)
}

func (f *NumberFaker) Float32(options ...opts.Optionable) float32 {
	return Numeric[float32](math.MaxFloat32, 0, options...)
}

func (f *NumberFaker) Float64(options ...opts.Optionable) float64 {
	return Numeric[float64](math.MaxFloat64, 0, options...)
}

func (f *NumberFaker) Binary(options ...opts.Optionable) string {
	return strconv.FormatInt(f.Int64(), 2)
}

func (f *NumberFaker) Octal(options ...opts.Optionable) string {
	return strconv.FormatInt(f.Int64(), 8)
}
