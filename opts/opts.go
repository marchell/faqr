package opts

type Optionable interface {
	Key() string
	Value() any
}

type Option struct {
	name  string
	value any
}

func (o Option) Key() string {
	return o.name
}
func (o Option) Value() any {
	return o.Value
}

func Max(max int64) Optionable {
	return Option{
		name:  "max",
		value: max,
	}
}

func Min(min int64) Optionable {
	return Option{
		name:  "min",
		value: min,
	}
}

func Length(length uint) Optionable {
	return Option{
		name:  "length",
		value: length,
	}
}

func Exclude(exclude []string) Optionable {
	return Option{
		name:  "exclude",
		value: exclude,
	}
}


type Case int
const (
	Lower Case = iota
	Upper
	Mixed
)

func (c Case) String() string {
	return [...]string{"lower", "upper", "mixed"}[c]
}

func Casing(casing Case) Optionable {
	return Option{
		name:  "casing",
		value: casing.String(),
	}
}

func Count(count uint) Optionable {
	return Option{
		name:  "count",
		value: count,
	}
}

func Prefix(prefix string) Optionable {
	return Option{
		name:  "prefix",
		value: prefix,
	}
}

func AllowLeadingZeros(allow bool) Optionable {
	return Option{
		name:  "allowLeadingZeros",
		value: allow,
	}
}


