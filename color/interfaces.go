package color

import (
	"gitlab.com/marchell/faqr/opts"
)

type Color interface {
	// Returns a CMYK color in decimal format.
	Cmyk(options ...opts.Optionable) []int

	// Returns an HSL color.
	// opts["includeAlpha"] - boolean, Adds an alpha value to the color (RGBA). Default false
	Hsl(options ...opts.Optionable) []int

	// Returns a random human readable color name.
	Human() string

	// Returns an HWB color.
	Hwb(options ...opts.Optionable) []int

	// Returns a LAB (CIELAB) color.
	Lab(options ...opts.Optionable) []int

	// Returns an LCH color. Even though upper bound of chroma in LCH color space is theoretically unbounded, it is bounded to 230 as anything above will not make a noticeable difference in the browser.
	Lch(options ...opts.Optionable) []int

	// Returns an RGB color.
	// opts["includeAlpha"] - boolean, Adds an alpha value to the color (RGBA). Default false
	Rgb(options ...opts.Optionable) []int

	// Returns an RGB color hex.
	// opts["includeAlpha"] - boolean, Adds an alpha value to the color (RGBA). Default false
	// opts["casing"] - string, Casing of the hex value. Default "lower"
	// opts["prefix"] - string, Prefix of the hex value. Default "#"
	RgbHex(options ...opts.Optionable) string
}
