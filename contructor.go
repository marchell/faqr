package faqr

import (
	"math/rand"
	"time"

	"gitlab.com/marchell/faqr/number"
	faqr_time "gitlab.com/marchell/faqr/time"
)

type Params struct {
	Locale []string
	Seed   int64
}

type faqr struct {
	// accept en only for now
	// ISO 639-1 maybe?
	// first locale that has a definition for a given property will be used.
	// TODO: actually use this
	Locale []string

	Rand *rand.Rand

	NowModifier   *time.Time
	NowModifierFn func() time.Time
}

func NewFaqr() Faqr {
	return NewFaqrWithParams(Params{})
}

func NewFaqrWithParams(params Params) Faqr {
	defaultParams := Params{
		Locale: []string{"en"},
		Seed:   time.Now().UnixNano(),
	}

	if params.Locale != nil && len(params.Locale) > 0 {
		defaultParams.Locale = params.Locale
	}

	if params.Seed > 0 {
		defaultParams.Seed = params.Seed
	}

	return &faqr{
		Locale: defaultParams.Locale,
		Rand:   rand.New(rand.NewSource(defaultParams.Seed)),
	}
}

func (faqr *faqr) Seed(seed int64) {
	faqr.Rand = rand.New(rand.NewSource(seed))
}

func (faqr *faqr) ReferenceTime(now time.Time) {
	faqr.NowModifier = &now
	faqr.NowModifierFn = nil
}

func (faqr *faqr) ReferenceTimeFn(fn func() time.Time) {
	faqr.NowModifier = nil
	faqr.NowModifierFn = fn
}

func (faqr *faqr) Time() faqr_time.Time {
	return &faqr_time.TimeFaker{}
}

func (faqr *faqr) Number() number.Number {
	return &number.NumberFaker{}
}
