package string

import (
	"fmt"

	"gitlab.com/marchell/faqr/opts"
	"gitlab.com/marchell/faqr/utils"
	"gitlab.com/marchell/faqr/helper"
)

type StringFaker struct{
	Number number.Number
}

// Generating a string consisting of letters in the English alphabet.
func (f *StringFaker) Alpha(options ...opts.Optionable) string {
	optionMap := utils.FilterMapByKey(
		utils.OptionsToMap(options),
		"casing",
		"exclude",
		"length",
		"max",
		"min",
	)

	fmt.Print(optionMap)

	return ""
}

// Generating a string consisting of alpha characters and digits.
func (f *StringFaker) AlphaNumeric(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// Returns a binary string.
func (f *StringFaker) Binary(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// Generates a string from the given characters.
func (f *StringFaker) FromCharacters(characters string, options ...opts.Optionable) string {
	optionMap := utils.FilterMapByKey(
		utils.OptionsToMap(options),
		"casing",
		"exclude",
		"length",
		"max",
		"min",
	)

	helper.NewHelper().Multiple(func() string {



	return ""
}

// Returns a hexadecimal string.
func (f *StringFaker) Hexadecimal(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// Generates a Nano ID.
func (f *StringFaker) NanoID(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// Generates a numeric string.
func (f *StringFaker) Numeric(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// Returns an octal string.
func (f *StringFaker) Octal(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// Returns a string containing UTF-16 chars between 33 and 126 (! to ~).
func (f *StringFaker) Sample(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// Returns a string containing only special characters from the following list:
// ! " # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \ ] ^ _ ` { | } ~
func (f *StringFaker) Symbol(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// Returns a UUID v4 (Universally Unique Identifier).
func (f *StringFaker) UUID() string {
	panic("not implemented") // TODO: Implement
}
