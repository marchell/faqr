package string

import "gitlab.com/marchell/faqr/opts"

const (
	CasingLower = "lower"
	CasingUpper = "upper"
	CasingMixed = "mixed"
)

type String interface {
	// Generating a string consisting of letters in the English alphabet.
	Alpha(options ...opts.Optionable) string

	// Generating a string consisting of alpha characters and digits.
	AlphaNumeric(options ...opts.Optionable) string

	// Returns a binary string.
	Binary(options ...opts.Optionable) string

	// Generates a string from the given characters.
	FromCharacters(characters string, options ...opts.Optionable) string

	// Returns a hexadecimal string.
	Hexadecimal(options ...opts.Optionable) string

	// Generates a Nano ID.
	NanoID(options ...opts.Optionable) string

	// Generates a numeric string.
	Numeric(options ...opts.Optionable) string

	// Returns an octal string.
	Octal(options ...opts.Optionable) string

	// Returns a string containing UTF-16 chars between 33 and 126 (! to ~).
	Sample(options ...opts.Optionable) string

	// Returns a string containing only special characters from the following list:
	// ! " # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \ ] ^ _ ` { | } ~
	Symbol(options ...opts.Optionable) string

	// Returns a UUID v4 (Universally Unique Identifier).
	UUID() string
}
