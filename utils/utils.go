package utils

import "gitlab.com/marchell/faqr/opts"

// OptionsToMap generates a map from a slice of Optionable objects.
//
// The function takes in a slice of Optionable objects called 'options'. It
// iterates over each 'option' in the 'options' slice and adds it to the
// 'optionMap' map using the 'Key()' method of the 'option' object as the key
// and the 'Value()' method of the 'option' object as the value. The function
// then returns the resulting 'optionMap'.
//
// The function returns a map[string]any, which is a map with string keys and
// any value type.
func OptionsToMap(options []opts.Optionable) map[string]any {
	optionMap := make(map[string]any)
	for _, option := range options {
		optionMap[option.Key()] = option.Value()
	}

	return optionMap
}

// FilterMapByKey filters a map by the specified keys and returns a new map containing only the key-value pairs
// that match the specified keys.
//
// The first parameter, m, is the original map that will be filtered.
// The second parameter, keys, is a variadic parameter that specifies the keys to filter the map by.
// The function returns a new map of the same type as the original map, containing only the key-value pairs
// that match the specified keys.
func FilterMapByKey[T comparable](m map[T]any, keys ...T) map[T]any {
	filteredMap := make(map[T]any)
	filter := make(map[T]any)

	for _, value := range keys {
		filter[value] = nil
	}

	// only getting the requested ones
	for k := range filter {
		if value, found := m[k]; found {
			filteredMap[k] = value
		}
	}

	return filteredMap
}
