package word

import (
	"gitlab.com/marchell/faqr/opts"
)

type Word interface {
	// Returns a string containing a random adjective.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Adjective(opts ...opts.Optionable) string

	// Returns a string containing a random adverb.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Adverb(opts ...opts.Optionable) string

	// Returns a string containing a random Conjunction.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Conjunction(opts ...opts.Optionable) string

	// Returns a string containing a random Interjection.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Interjection(opts ...opts.Optionable) string

	// Returns a string containing a random Noun.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Noun(opts ...opts.Optionable) string

	// Returns a string containing a random Preposition.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Preposition(opts ...opts.Optionable) string

	// Returns a string containing a random Sample.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Sample(opts ...opts.Optionable) string

	// Returns a string containing a random Verb.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word
	Verb(opts ...opts.Optionable) string

	// Returns a string containing a random adjective.
	// opts["length"] - the word's length
	// opts["min"] and opts["max"] - the word's length range, min and max inclusive, only used when length is not set
	// opts["strategy"] - The strategy to apply when no words with a matching length are found. Available error handling strategies:
	// - "throw" - throw an error
	// - "shortest" - return the shortest word
	// - "closest" - return the closest word to the given length, prioritizing the shortest word if there is a tie
	// - "longest" - return the longest word
	// - "random" - return a random word

	// Returns a string containing a number of space separated random words.
	//
	// opts["count"] - the number of words to return
	// opts["min"] and opts["max"] - the number of words to return, min and max inclusive, only used when count is not set
	Words(opts ...opts.Optionable) []string
}
