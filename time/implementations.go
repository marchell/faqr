package time

import (
	"time"

	"gitlab.com/marchell/faqr/opts"
)

type TimeFaker struct{}

// Any returns a random time.Time value
func (f *TimeFaker) Any() time.Time {
	return time.Now()
}

// generate a random date between two dates
func (f *TimeFaker) Between(options ...opts.Optionable) time.Time {
	panic("not implemented") // TODO: Implement
}

// generate multiple random date between two dates
func (f *TimeFaker) Betweens(options ...opts.Optionable) time.Time {
	panic("not implemented") // TODO: Implement
}

// generate a random birthdate
func (f *TimeFaker) Birthdate(options ...opts.Optionable) time.Time {
	panic("not implemented") // TODO: Implement
}

// generate a random date in the future
func (f *TimeFaker) Future(options ...opts.Optionable) time.Time {
	panic("not implemented") // TODO: Implement
}

// generate random name of a month
func (f *TimeFaker) Month(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}

// generate a random date in the past
func (f *TimeFaker) Past(options ...opts.Optionable) time.Time {
	panic("not implemented") // TODO: Implement
}

// generate a random date in the recent past
func (f *TimeFaker) Recent(options ...opts.Optionable) time.Time {
	panic("not implemented") // TODO: Implement
}

// generate a random date in the recent past
func (f *TimeFaker) Soon(options ...opts.Optionable) time.Time {
	panic("not implemented") // TODO: Implement
}

// generate random name of a month
func (f *TimeFaker) Weekday(options ...opts.Optionable) string {
	panic("not implemented") // TODO: Implement
}
