package time

import (
	"time"

	"gitlab.com/marchell/faqr/opts"
)

type Time interface {
	// generate a random date
	Any() time.Time

	// generate a random date between two dates
	Between(options ...opts.Optionable) time.Time

	// generate multiple random date between two dates
	Betweens(options ...opts.Optionable) time.Time

	// generate a random birthdate
	Birthdate(options ...opts.Optionable) time.Time

	// generate a random date in the future
	Future(options ...opts.Optionable) time.Time

	// generate random name of a month
	Month(options ...opts.Optionable) string

	// generate a random date in the past
	Past(options ...opts.Optionable) time.Time

	// generate a random date in the recent past
	Recent(options ...opts.Optionable) time.Time

	// generate a random date in the recent past
	Soon(options ...opts.Optionable) time.Time

	// generate random name of a month
	Weekday(options ...opts.Optionable) string
}
